/* This is the main.js file */

// Menu Active
var nav = document.getElementsByClassName('navbar__link');
var currentLocation = location.href;
for(let i = 0; i < nav.length; i++) {
    nav[i].classList.remove("active")
    if(nav[i].href === currentLocation){
        nav[i].classList.add('active');
    }
}
// Menggunakan JQUERY dan terdapat CDN di header html
$(document).ready(function(){
    // Membuat pada saat card dklik maka overvlow hidden pada body, Jika menampilkan OverflowX:scroll maka tambhakan di css pada class workingspace__overlay:target tambahkan property overflowX jadi hidden.
    $(".popup").click(function(){
        $("body").css("overflowY", "hidden");
        $("body").css("overflowX", "hidden")
    })
    // Dan pada tombol close d klik, maka overflowY:hidden
    $(".close").click(function(){
        $("body").css("overflow", "scroll");
        // $("body").css("overflowX", "hidden");
    })
})
